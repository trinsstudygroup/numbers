/*
	Name: Chris Hurst
	Class: CSC160
	Due Date: 10/26/15
	Files: numbers.cpp
	Description: Program compiled of two functions, that will run numbers from 0-10000 to
	find the perfect numbers and print those to the screen, and then also ask for the user 
	input a integer and let them know if it is a prime or not prime number.  
*/
#include <iostream>
#include <stdlib.h>

using namespace std;

void isPerfect();
int isPrime();

int main(){

    char repeat = 'y';

    isPerfect();

    while(repeat == 'y'){
        isPrime();
        cout << "\n\nWould you like to do another? (y/n): " << endl;
        cin >> repeat;
        repeat = tolower(repeat);
    }
}

void isPerfect () {



    for (int range = 2; range <= 10000; range++) { //loop for all numbers_program_4 0 - 10000
        int divisor = range / 2;
        int sum = 0;
        for (int perfect = 1; perfect <= divisor ; perfect++) { //inner loop takes range == sum to find perfect
            if (range % perfect == 0) {
                sum += perfect;
            }
        }
        if (sum == range) {
            cout << range << " is a perfect number. It's divsors are: " << endl;
            for (int n = 1; n < range; n++){
                if (range % n == 0) {
                    cout << n << " ";
                }
            }
            cout << endl;
            cout << "Press any key to continue . . . "; // sys pause
            cin.ignore(50, '\n'); // sys pause
            cout << string(50 , '\n'); // sys clear
        }
    }

}

int isPrime() {

    int number;
    bool x = true;
    char restart = 'y';

    while(restart == 'y') {
        cout << "Please enter a positive integer and I will tell you if it is prime: ";

        while (!(cin >> number) || number < 0) {
            if (!cin) {
                cout << "\nERROR: You have entered an invalid character." << endl;
                cout << "Please enter a positive number greater than zero (0): ";
                cin.clear();
                cin.ignore(200, '\n');
                return x;
            }
            else
                cout << "\nERROR: Please enter a number that is greater than zero: ";
            return x;
        }
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                x = false;
            }
        }
        if (x == false) {
            cout << endl;
            cout << number << " is not prime";
            return x;
        }
        else {
            cout << endl;
            cout << number << " is prime";
        }
        return x;
    }
}



